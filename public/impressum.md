---
subject: Impressum
title: Impressum
---

# Impressum

Angaben gemäß § 5 TMG:

Claudia Pellegrino  
Adelungstraße 55  
64283 Darmstadt

Telefon: +49 6151 1300123  
E-Mail: cp@tiqua.de

## Streitschlichtung

An Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle brauche ich als private Diensteanbieterin nicht teilzunehmen und werde das auch nicht.

## Haftung für Inhalte

Als Diensteanbieterin bin ich gemäß § 7 Abs. 1 TMG für eigene Inhalte auf diesen Seiten verantwortlich. Nach §§ 8 bis 10 TMG brauche ich Informationen, die auf meine Dienste übermittelt oder dort gespeichert werden, weder zu überwachen noch sie auf mögliche Rechtsverstöße prüfen.

Da, wo es das Gesetz verlangt, kann es allerdings sein, dass ich Inhalte entfernen oder sperren muss. Das tut mir Leid. Egal wie: ich werde das erst dann tun, wenn ich von der konkreten Rechtsverletzung Kenntnis habe. Bis dahin schließe ich eine Haftung aus. Wer etwas anderes erwartet, sollte sich schämen und mal in Ruhe darüber nachdenken.

## Haftung für Links

Mein Angebot enthält Links zu anderen Websites. Weil ich auf deren Inhalte keinen Einfluss habe, übernehme ich auch keine Gewähr oder Haftung dafür. Bevor ich etwas verlinke, prüfe ich immer, ob ich dort auf den ersten Blick irgendetwas sehe, was ich für rechtswidrig halte. Wenn eine Seite aber einmal verlinkt ist, dann gucke ich sie mir auch nicht mehr an – es sei denn, jemand weist mich auf eine mögliche Rechtsverletzung hin. Dann prüfe ich den Link nochmal und lösche ihn gegebenenfalls.

## Urheberrecht

Die Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Für Inhalte, bei denen eine Lizenz angegeben ist, gilt die Lizenz. Für alle anderen Inhalte gilt: Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung der Personen, die die Inhalte erstellt haben.

Ich beachte die Urheberrechte Dritter. Wo nötig, kennzeichne ich Inhalte Dritter als solche. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, dann seien Sie so freundlich und weisen mich bitte kurz darauf hin. Ich prüfe dann den Inhalt und entferne ihn gegebenenfalls.

## Quelle

[e-recht24.de](https://www.e-recht24.de) (sprachlich und inhaltlich überarbeitet)
