---
subject: Datenschutzerklärung
title: Datenschutzerklärung
---

# Datenschutzerklärung

Der Schutz Ihrer persönlichen Daten liegt mir, der Betreiberin dieser Seite, am Herzen. Ich behandele Ihre personenbezogenen Daten vertraulich und halte mich an die Regeln, die das Gesetz und diese Datenschutzerklärung vorschreibt.

Die meisten Dienste auf meinen Seiten können Sie nutzen, ohne dass Sie dafür personenbezogene Daten angeben müssen. Soweit auf meinen Seiten personenbezogene Daten erhoben werden – beispielsweise Name, Anschrift oder E-Mail-Adressen – so erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Ich gebe Ihre Daten niemals an Dritte weiter, wenn Sie dem nicht ausdrücklich zustimmen.

Ich weise darauf hin, dass die Software und die Protokolle, die bei internetbasierten Diensten zur Anwendung kommen, Sicherheitslücken aufweisen können. Ich warte meine Systeme sorgfältig und halte sie sicherheitstechnisch auf dem neuesten Stand. Damit sorge ich dafür, dass Ihre Daten nur mit äußerst geringer Wahrscheinlichkeit abfließen oder von Dritten eingesehen werden können. Einen lückenlosen Schutz der Daten vor dem Zugriff durch Dritte gibt es jedoch nicht.

## Cookies

Meine Dienste verwenden hin und wieder Cookies. Das sind kleine Datenpakete, die Ihr Browser auf Ihrem Gerät ablegt. Cookies richten keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, mein Angebot nutzerfreundlicher, effektiver und sicherer zu machen.

Wenn ich Cookies verwende, dann sind das meistens so genannte Session-Cookies. Diese werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies wiederum bleiben auf Ihrem Gerät gespeichert, bis Sie diese löschen. Diese Cookies ermöglichen es meinem Dienst, Ihren Browser – und damit Sie – beim nächsten Besuch wiederzuerkennen.

Wenn Sie einen modernen Browser haben, dann können Sie ihn so einstellen, dass er Sie über das Setzen von Cookies im Einzelfall oder generell informiert. Außerdem können Sie Ihren Browser generell, fallweise oder domainbezogen anweisen, dass er Cookies annehmen darf, ablehnen muss oder sie am Ende Ihrer Browsersitzung automatisch löscht. Falls Sie sich dazu entschließen Cookies für meine Dienste abzuschalten, dann ist das OK. Es kann dann höchstens passieren, dass die Dienste auf Ihrem Gerät nicht mehr toll oder gar nicht mehr funktionieren.

## Server-Logdateien

Die Software, mit der ich die Dienste betreibe, erhebt und speichert automatisch in Server-Logdateien bestimmte Informationen, die Ihr Browser automatisch an den von mir betriebenen Dienst übermittelt. Dies sind:

- Browsertyp und Browserversion
- verwendetes Betriebssystem
- Referrer-URL (das ist die Website, die Sie zuvor besucht haben)
- Hostname oder IP-Adresse des Gerätes, das Sie benutzen
- Datum und Uhrzeit der Nutzung

Diese Daten sind nicht dafür geeignet oder dafür gedacht, mit bestimmen Personen oder anderen Datenquellen verknüpft zu werden. Entsprechend werde ich das auch nicht tun. Sollte ich einen konkreten Anhaltspunkt haben, dass jemand die Dienste rechtswidrig nutzt, dann behalte ich mir vor die gespeicherten Daten im Nachhinein zu prüfen.

## Kommentar- und andere soziale Funktionen

Immer da, wo meine Dienste eine Kommentar- oder anderweitig soziale Funktion anbieten, werden neben Ihrem Kommentar auch andere Angaben gespeichert. Das gilt auch für Dienste, die dafür gedacht sind Softwareprojekte und Versionsstände zu verwalten (z. B. Git).

Folgende Daten werden im Zusammenhang mit den genannten Diensten erhoben und gespeichert:

1. die von Ihnen verfassten Werke, Quellcode, Kommentare, Reaktionen, Commit-Anmerkungen und Review-Anmerkungen;
2. der Zeitpunkt, an dem Sie Ihren unter Nr. 1 genannten Beitrag verfasst haben;
3. der Zeitpunkt, an dem Sie Ihren unter Nr. 1 genannten Beitrag an den Dienst geschickt haben;
4. Ihr Benutzername – falls Sie Ihren Beitrag nicht anonym eingereicht haben – und
5. Ihre E-Mail-Adresse.

## Speicherung der IP-Adresse

Alle Kommentar- und sonstige sozialen Funktionen speichern die IP-Adressen der Nutzer, die Beiträge verfassen. Da ich Beiträge auf unserer Seite nicht vor der Freischaltung prüfe, benötige ich diese gespeicherten Daten, damit ich bei Rechtsverletzungen oder Straftaten zum Beispiel Ihren Internet-Provider benachrichtigen kann (Abuse-Meldung). Wenn Sie vorhin gut aufgepasst haben, dann wissen Sie bestimmt noch: Verknüpfungen zwischen IP-Adresse und Person gibt es nicht. Genau aus diesem Grund gebe ich gespeicherte IP-Adressen auch nicht an Ermittlungsbehörden heraus, es sei denn, eine richterliche Anordnung zwingt mich dazu.

## Abonnieren von Kommentaren

Manche Dienste bieten eine Funktion an, mit der Sie sich über neue Kommentare oder ähnliche Ereignisse benachrichtigen lassen können. Nach Anmeldung erhalten Sie anfangs eine Bestätigungs-E-Mail. Diese ist notwendig um zu prüfen, ob die angegebene E-Mail-Adresse Ihnen wirklich gehört (Double-Opt-In). Sie können diese Funktion jederzeit abbestellen. Aufgrund eines technischen Problems oder Versehens kann es vorkommen, dass ein Abmelde-Link mal nicht funktioniert oder ganz fehlt. In diesem Fall wenden Sie sich bitte [telefonisch oder per E-Mail an mich](/impressum). Ich werde Ihrem Abmeldewunsch dann unverzüglich nachkommen.

## TLS-Verschlüsselung

Meine Dienste nutzen TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass Ihr Browser eine Adresse anzeigt, die mit `https://` beginnt.

Bei aktiver TLS-Verschlüsselung können die Daten, die Sie mit den Diensten austauschen, im Regelfall nicht von Dritten mitgelesen werden. Manche Rechner (z. B. Bürorechner) sind allerdings mit Absicht so präpariert, dass bestimmte Dritte (z. B. die IT-Abteilung) die genannten Daten entschlüsseln und einsehen können. Falls Sie Ihren Rechner nicht selbst administrieren, dann erkundigen Sie sich bitte bei Ihrem Administrator, ob diese Situation für Ihr Gerät oder Ihr Netzwerk zutrifft.

## Recht auf Auskunft, Löschung, Sperrung

Sie haben jederzeit das Recht kostenfrei Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung zu bekommen. Außerdem haben Sie das Recht, dass diese Daten berichtigt, gesperrt oder gelöscht werden. Falls Sie dies wünschen, so wenden Sie sich bitte [an mich](/impressum).

## Bitte keine Werbung zusenden

Hiermit widerspreche ich der Nutzung meiner Kontaktdaten, die ich im Rahmen der Impressumspflicht veröffentlicht habe, um mir Werbung oder Infos zu schicken, die ich nicht angefordert habe. Ich behalte mir vor, dass ich auf derartiges unverlangtes Material mit rechtlichen Schritten reagiere.

## Quelle

[e-recht24.de](https://www.e-recht24.de) (sprachlich und inhaltlich überarbeitet)
